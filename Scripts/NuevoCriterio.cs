using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class NuevoCriterio : MonoBehaviour
{
    public GameObject criterio, contenidoCrit, contenidoApartado;
    public InputField crit, ncriterio;
    public float contenedor, contenedor2;
    private void Start()
    {
        this.gameObject.name = "NuevoCrit";
        crit.text = "";
    }

    public void GestionarNuevoCriterio()
    {
        contenedor = contenidoCrit.transform.GetComponent<RectTransform>().sizeDelta.y;
        contenedor2 = contenidoApartado.transform.GetComponent<RectTransform>().sizeDelta.y;
        contenidoCrit.GetComponent<RectTransform>().SetSizeWithCurrentAnchors(RectTransform.Axis.Vertical, (contenedor + 70));
        contenidoApartado.GetComponent<RectTransform>().SetSizeWithCurrentAnchors(RectTransform.Axis.Vertical, (contenedor2 + 70));
        Instantiate(criterio, contenidoApartado.transform, false);

        transform.parent.parent.parent.name = crit.text;
        ncriterio.text = crit.text;
        ncriterio.name = "n" + crit.text;
        Destroy(transform.parent.parent.gameObject);
    }

}
