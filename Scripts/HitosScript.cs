using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class HitosScript : MonoBehaviour
{
    public GameObject objetivos, contenidoH2, contenidoH3, contenidoH4, criterios;
    public bool inicio = false, finH1 = false, finH2 = false, finH3 = false, finH4 = false;
    public Text fecha1, fecha2, fecha3, fecha4;
    public DateTime fechaInicial;
    public float contenedor, contenedor2, progH1, progH2, progH3, progH4, aux;
    public int obTotalH1, obTotalH2, obTotalH3, obTotalH4;
    public InputField panel1, panel2, panel3, panel4;


    void Update()
    {
        if (Input.GetKeyDown(KeyCode.H))
        {
            int cant1 = objetivos.GetComponent<Transform>().GetChild(0).transform.childCount - 2;
            int cant2 = objetivos.GetComponent<Transform>().GetChild(1).transform.childCount - 2;
            int cant3 = objetivos.GetComponent<Transform>().GetChild(2).transform.childCount - 2;
            int cant4 = objetivos.GetComponent<Transform>().GetChild(3).transform.childCount - 2;
        }

        if (!inicio && this.gameObject.name != "HitosCanvas")
        {
            fechaInicial = DateTime.Now;
            fecha1.text = fechaInicial.AddDays(15).ToShortDateString();
            fecha2.text = fechaInicial.AddDays(30).ToShortDateString();
            fecha3.text = fechaInicial.AddDays(45).ToShortDateString();
            fecha4.text = fechaInicial.AddDays(60).ToShortDateString();

            PlayerPrefs.SetFloat(this.transform.GetChild(0).name + "progH1", 0);
            PlayerPrefs.SetFloat(this.transform.GetChild(0).name + "progH2", 0);
            PlayerPrefs.SetFloat(this.transform.GetChild(0).name + "progH3", 0);
            PlayerPrefs.SetFloat(this.transform.GetChild(0).name + "progH4", 0);
            inicio = true;
        }

        if (inicio && this.gameObject.name != "HitosCanvas")
        {
            if (!finH1)
            {
                aux = 0;
                obTotalH1 = objetivos.GetComponent<Transform>().GetChild(0).transform.childCount - 2;
                for (int i = 0; i < obTotalH1; i++)
                {
                    if (objetivos.GetComponent<Transform>().GetChild(0).GetChild(i + 1).GetChild(1).GetChild(0).GetComponent<Toggle>().isOn)
                    {
                        aux++;
                    }
                }
                aux /= obTotalH1;
                if (progH1 != aux * 100)
                {
                    progH1 = aux * 100;
                }
                if (PlayerPrefs.GetFloat(this.transform.GetChild(0).name + "progH1") != progH1)
                {
                    PlayerPrefs.SetFloat(this.transform.GetChild(0).name + "progH1", progH1);
                }
            }

            if (!finH2)
            {
                aux = 0;
                obTotalH2 = objetivos.GetComponent<Transform>().GetChild(1).transform.childCount - 2;
                for (int i = 0; i < obTotalH2; i++)
                {
                    if (objetivos.GetComponent<Transform>().GetChild(1).GetChild(i + 1).GetChild(1).GetChild(0).GetComponent<Toggle>().isOn)
                    {
                        aux++;
                    }
                }
                aux /= obTotalH2;
                if (progH2 != aux * 100)
                {
                    progH2 = aux * 100;
                }
                if (PlayerPrefs.GetFloat(this.transform.GetChild(0).name + "progH2") != progH2)
                {
                    PlayerPrefs.SetFloat(this.transform.GetChild(0).name + "progH2", progH2);
                }
            }

            if (!finH3)
            {
                aux = 0;
                obTotalH3 = objetivos.GetComponent<Transform>().GetChild(2).transform.childCount - 2;
                for (int i = 0; i < obTotalH3; i++)
                {
                    if (objetivos.GetComponent<Transform>().GetChild(2).GetChild(i + 1).GetChild(1).GetChild(0).GetComponent<Toggle>().isOn)
                    {
                        aux++;
                    }
                }
                aux /= obTotalH3;
                if (progH3 != aux * 100)
                {
                    progH3 = aux * 100;
                }
                if (PlayerPrefs.GetFloat(this.transform.GetChild(0).name + "progH3") != progH3)
                {
                    PlayerPrefs.SetFloat(this.transform.GetChild(0).name + "progH3", progH3);
                }
            }


            if (!finH4)
            {
                aux = 0;
                obTotalH4 = objetivos.GetComponent<Transform>().GetChild(3).transform.childCount - 2;
                for (int i = 0; i < obTotalH4; i++)
                {
                    if (objetivos.GetComponent<Transform>().GetChild(3).GetChild(i + 1).GetChild(1).GetChild(0).GetComponent<Toggle>().isOn)
                    {
                        aux++;
                    }
                }
                aux /= obTotalH4;
                if (progH4 != aux * 100)
                {
                    progH4 = aux * 100;
                }

                if (PlayerPrefs.GetFloat(this.transform.GetChild(0).name + "progH4") != progH4)
                {
                    PlayerPrefs.SetFloat(this.transform.GetChild(0).name + "progH4", progH4);
                }
            }
        }
        if (!finH1 && inicio && fechaInicial.AddDays(15) < DateTime.Now && this.gameObject.name != "HitosCanvas")
        {
            finH1 = true;
            obTotalH1 = objetivos.GetComponent<Transform>().GetChild(0).transform.childCount - 2;

            objetivos.transform.GetChild(0).GetChild(0).GetChild(0).GetComponent<InputField>().interactable = false;

            objetivos.transform.GetChild(0).GetChild(obTotalH1 + 1).gameObject.SetActive(false);
            for (int i = 0; i < obTotalH1; i++)
            {

                if (!objetivos.GetComponent<Transform>().GetChild(0).GetChild(i + 1).GetChild(1).GetChild(0).GetComponent<Toggle>().isOn)
                {
                    Instantiate(objetivos.GetComponent<Transform>().GetChild(0).GetChild(i + 1).gameObject, objetivos.GetComponent<Transform>().GetChild(1).transform, false);
                    objetivos.transform.GetChild(1).GetChild(objetivos.transform.GetChild(1).transform.childCount - 1).SetSiblingIndex(objetivos.transform.GetChild(1).GetChild(objetivos.transform.GetChild(1).transform.childCount - 1).GetSiblingIndex() - 1);
                    objetivos.transform.GetChild(0).GetChild(i + 1).GetChild(0).GetChild(1).transform.Find("critxt").GetComponent<Text>().color = Color.red;
                    objetivos.transform.GetChild(0).GetChild(i + 1).GetChild(0).GetChild(1).GetComponent<InputField>().interactable = false;
                    objetivos.transform.GetChild(0).GetChild(i + 1).GetChild(1).GetChild(0).GetComponent<Toggle>().enabled = false;
                    contenedor = contenidoH2.transform.GetComponent<RectTransform>().sizeDelta.y;
                    contenedor2 = objetivos.transform.GetComponent<RectTransform>().sizeDelta.y;
                    contenidoH2.GetComponent<RectTransform>().SetSizeWithCurrentAnchors(RectTransform.Axis.Vertical, (contenedor + 70));
                    objetivos.GetComponent<RectTransform>().SetSizeWithCurrentAnchors(RectTransform.Axis.Vertical, (contenedor2 + 70));
                }
                else
                {
                    objetivos.transform.GetChild(0).GetChild(i + 1).GetChild(0).GetChild(1).transform.Find("critxt").GetComponent<Text>().color = Color.green;
                    objetivos.transform.GetChild(0).GetChild(i + 1).GetChild(0).GetChild(1).GetComponent<InputField>().interactable = false;
                    objetivos.transform.GetChild(0).GetChild(i + 1).GetChild(1).GetChild(0).GetComponent<Toggle>().enabled = false;
                }
            }
        }

        if (!finH2 && inicio && fechaInicial.AddDays(30) < DateTime.Now && this.gameObject.name != "HitosCanvas")
        {
            finH2 = true;
            obTotalH2 = objetivos.GetComponent<Transform>().GetChild(1).transform.childCount - 2;

            objetivos.transform.GetChild(1).GetChild(0).GetChild(0).GetComponent<InputField>().interactable = false;
            objetivos.transform.GetChild(1).GetChild(obTotalH2 + 1).gameObject.SetActive(false);

            for (int i = 0; i < obTotalH2; i++)
            {

                if (!objetivos.GetComponent<Transform>().GetChild(1).GetChild(i + 1).GetChild(1).GetChild(0).GetComponent<Toggle>().isOn)
                {

                    Instantiate(objetivos.GetComponent<Transform>().GetChild(1).GetChild(i + 1).gameObject, objetivos.GetComponent<Transform>().GetChild(2).transform, false);
                    objetivos.transform.GetChild(2).GetChild(objetivos.transform.GetChild(2).transform.childCount - 1).SetSiblingIndex(objetivos.transform.GetChild(2).GetChild(objetivos.transform.GetChild(2).transform.childCount - 1).GetSiblingIndex() - 1);
                    objetivos.transform.GetChild(1).GetChild(i + 1).GetChild(0).GetChild(1).transform.Find("critxt").GetComponent<Text>().color = Color.red;
                    objetivos.transform.GetChild(1).GetChild(i + 1).GetChild(0).GetChild(1).GetComponent<InputField>().interactable = false;
                    objetivos.transform.GetChild(1).GetChild(i + 1).GetChild(1).GetChild(0).GetComponent<Toggle>().enabled = false;
                    contenedor = contenidoH3.transform.GetComponent<RectTransform>().sizeDelta.y;
                    contenedor2 = objetivos.transform.GetComponent<RectTransform>().sizeDelta.y;
                    contenidoH3.GetComponent<RectTransform>().SetSizeWithCurrentAnchors(RectTransform.Axis.Vertical, (contenedor + 70));
                    objetivos.GetComponent<RectTransform>().SetSizeWithCurrentAnchors(RectTransform.Axis.Vertical, (contenedor2 + 70));
                }
                else
                {
                    objetivos.transform.GetChild(1).GetChild(i + 1).GetChild(0).GetChild(1).transform.Find("critxt").GetComponent<Text>().color = Color.green;
                    objetivos.transform.GetChild(1).GetChild(i + 1).GetChild(0).GetChild(1).GetComponent<InputField>().interactable = false;
                    objetivos.transform.GetChild(1).GetChild(i + 1).GetChild(1).GetChild(0).GetComponent<Toggle>().enabled = false;
                }
            }
        }

        if (!finH3 && inicio && fechaInicial.AddDays(45) < DateTime.Now && this.gameObject.name != "HitosCanvas")
        {
            finH3 = true;
            obTotalH3 = objetivos.GetComponent<Transform>().GetChild(2).transform.childCount - 2;

            objetivos.transform.GetChild(2).GetChild(0).GetChild(0).GetComponent<InputField>().interactable = false;
            objetivos.transform.GetChild(2).GetChild(obTotalH3 + 1).gameObject.SetActive(false);

            for (int i = 0; i < obTotalH3; i++)
            {

                if (!objetivos.GetComponent<Transform>().GetChild(2).GetChild(i + 1).GetChild(1).GetChild(0).GetComponent<Toggle>().isOn)
                {
                    Instantiate(objetivos.GetComponent<Transform>().GetChild(2).GetChild(i + 1).gameObject, objetivos.GetComponent<Transform>().GetChild(3).transform, false);
                    objetivos.transform.GetChild(3).GetChild(objetivos.transform.GetChild(3).transform.childCount - 1).SetSiblingIndex(objetivos.transform.GetChild(3).GetChild(objetivos.transform.GetChild(3).transform.childCount - 1).GetSiblingIndex() - 1);
                    objetivos.transform.GetChild(2).GetChild(i + 1).GetChild(0).GetChild(1).transform.Find("critxt").GetComponent<Text>().color = Color.red;
                    objetivos.transform.GetChild(2).GetChild(i + 1).GetChild(0).GetChild(1).GetComponent<InputField>().interactable = false;
                    objetivos.transform.GetChild(2).GetChild(i + 1).GetChild(1).GetChild(0).GetComponent<Toggle>().enabled = false;
                    contenedor = contenidoH3.transform.GetComponent<RectTransform>().sizeDelta.y;
                    contenedor2 = objetivos.transform.GetComponent<RectTransform>().sizeDelta.y;
                    contenidoH3.GetComponent<RectTransform>().SetSizeWithCurrentAnchors(RectTransform.Axis.Vertical, (contenedor + 70));
                    objetivos.GetComponent<RectTransform>().SetSizeWithCurrentAnchors(RectTransform.Axis.Vertical, (contenedor2 + 70));
                }
                else
                {
                    objetivos.transform.GetChild(2).GetChild(i + 1).GetChild(0).GetChild(1).transform.Find("critxt").GetComponent<Text>().color = Color.green;
                    objetivos.transform.GetChild(2).GetChild(i + 1).GetChild(0).GetChild(1).GetComponent<InputField>().interactable = false;
                    objetivos.transform.GetChild(2).GetChild(i + 1).GetChild(1).GetChild(0).GetComponent<Toggle>().enabled = false;
                }
            }
        }

        if (!finH4 && inicio && fechaInicial.AddDays(60) < DateTime.Now && this.gameObject.name != "HitosCanvas")
        {
            finH4 = true;
            obTotalH4 = objetivos.GetComponent<Transform>().GetChild(3).transform.childCount - 2;

            objetivos.transform.GetChild(3).GetChild(0).GetChild(0).GetComponent<InputField>().interactable = false;
            objetivos.transform.GetChild(3).GetChild(obTotalH4 + 1).gameObject.SetActive(false);


            for (int i = 0; i < obTotalH4; i++)
            {

                if (!objetivos.GetComponent<Transform>().GetChild(1).GetChild(i + 1).GetChild(1).GetChild(0).GetComponent<Toggle>().isOn)
                {
                    objetivos.transform.GetChild(3).GetChild(i + 1).GetChild(0).GetChild(1).transform.Find("critxt").GetComponent<Text>().color = Color.red;
                    objetivos.transform.GetChild(3).GetChild(i + 1).GetChild(0).GetChild(1).GetComponent<InputField>().interactable = false;
                    objetivos.transform.GetChild(3).GetChild(i + 1).GetChild(1).GetChild(0).GetComponent<Toggle>().enabled = false;
                }
                else
                {
                    objetivos.transform.GetChild(3).GetChild(i + 1).GetChild(0).GetChild(1).transform.Find("critxt").GetComponent<Text>().color = Color.green;
                    objetivos.transform.GetChild(3).GetChild(i + 1).GetChild(0).GetChild(1).GetComponent<InputField>().interactable = false;
                    objetivos.transform.GetChild(3).GetChild(i + 1).GetChild(1).GetChild(0).GetComponent<Toggle>().enabled = false;
                }
            }

            objetivos.transform.GetChild(4).gameObject.SetActive(true);
        }
    }

    public void Evaluar()
    {
        if (criterios.transform.GetChild(3).GetChild(0).GetChild(0).childCount <= 1)
        {
            Debug.Log("no se han establecido criterios de evaluacion");
        }
        else if (criterios.name != "CriteriosCanvas")
        {
            criterios.GetComponent<Canvas>().enabled = true;
        }
        else
        {
            Instantiate(criterios);
            criterios = GameObject.Find("CriteriosCanvas(Clone)").gameObject;
            criterios.name = "Evaluacion" + this.transform.GetChild(0).name;
            criterios.GetComponent<Canvas>().enabled = true;
            this.transform.parent.GetComponent<Canvas>().enabled = false;
            criterios.transform.GetChild(1).GetComponent<Text>().text = "Evaluación de " + this.transform.GetChild(0).name;
            criterios.transform.GetChild(2).GetChild(1).GetComponent<Text>().text = "Volver";

            contenedor = criterios.transform.GetChild(3).GetChild(0).GetChild(0).transform.GetComponent<RectTransform>().sizeDelta.y;
            criterios.transform.GetChild(3).GetChild(0).GetChild(0).GetComponent<RectTransform>().SetSizeWithCurrentAnchors(RectTransform.Axis.Vertical, contenedor - 90);
            foreach (Transform child in criterios.transform.GetChild(3).GetChild(0).GetChild(0))
            {
                child.transform.GetComponent<Button>().enabled = false;
                contenedor = child.transform.GetComponent<RectTransform>().sizeDelta.y;
                child.GetComponent<RectTransform>().SetSizeWithCurrentAnchors(RectTransform.Axis.Vertical, contenedor - 70);
                contenedor = criterios.transform.GetChild(3).GetChild(0).GetChild(0).transform.GetComponent<RectTransform>().sizeDelta.y;
                criterios.transform.GetChild(3).GetChild(0).GetChild(0).GetComponent<RectTransform>().SetSizeWithCurrentAnchors(RectTransform.Axis.Vertical, contenedor - 70);
                child.transform.GetChild(child.transform.childCount - 1).gameObject.SetActive(false);
                foreach (Transform hijo in child)
                {
                    if (hijo.transform.GetSiblingIndex() == 0 || hijo.transform.GetSiblingIndex() == child.transform.childCount - 1)
                    {
                    }
                    else
                    {
                        hijo.GetChild(2).gameObject.SetActive(true);
                    }

                    foreach (Transform nieto in hijo)
                    {
                        if (nieto.transform.childCount > 1)
                        {
                            nieto.GetChild(1).GetComponent<InputField>().interactable = false;

                        }
                        else
                        {
                        }
                    }
                }
            }
            criterios.transform.GetChild(3).GetChild(0).GetChild(0).GetChild(criterios.transform.GetChild(3).GetChild(0).GetChild(0).transform.childCount - 1).gameObject.SetActive(false);
            criterios.transform.GetChild(4).gameObject.SetActive(false);
            criterios.transform.GetChild(5).transform.gameObject.SetActive(true);
            contenedor = criterios.transform.GetChild(3).transform.GetComponent<RectTransform>().sizeDelta.y;
            criterios.transform.GetChild(3).GetComponent<RectTransform>().SetSizeWithCurrentAnchors(RectTransform.Axis.Vertical, contenedor - 60);
        }

    }


}
